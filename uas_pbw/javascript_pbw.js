window.addEventListener('load', () => {
	const form2 = document.querySelector("#new-aktivitas-form2");
	const input2 = document.querySelector("#new-aktivitas-input2");
	const list_el2 = document.querySelector("#tasks2");
    const form = document.querySelector("#new-kegiatan-form");
	const input = document.querySelector("#new-kegiatan-input");
	const list_el = document.querySelector("#tasks");
    

	form2.addEventListener('submit', (e) => {
		e.preventDefault();

		const aktivitas = input2.value;

		const aktivitas_el = document.createElement('div');
		aktivitas_el.classList.add('aktivitas');

		const aktivitas_content_el = document.createElement('div');
		aktivitas_content_el.classList.add('content');

		aktivitas_el.appendChild(aktivitas_content_el);

		const aktivitas_input2_el = document.createElement('input');
		aktivitas_input2_el.classList.add('text');
		aktivitas_input2_el.type = 'text';
		aktivitas_input2_el.value = aktivitas;
		aktivitas_input2_el.setAttribute('readonly', 'readonly');

		aktivitas_content_el.appendChild(aktivitas_input2_el);

		const aktivitas_actions_el = document.createElement('div');
		aktivitas_actions_el.classList.add('actions');
		
		const aktivitas_edit_el = document.createElement('button');
		aktivitas_edit_el.classList.add('edit');
		aktivitas_edit_el.innerText = 'Edit';

		const aktivitas_delete_el = document.createElement('button');
		aktivitas_delete_el.classList.add('delete');
		aktivitas_delete_el.innerText = 'hapus';

		aktivitas_actions_el.appendChild(aktivitas_edit_el);
		aktivitas_actions_el.appendChild(aktivitas_delete_el);

		aktivitas_el.appendChild(aktivitas_actions_el);

		list_el2.appendChild(aktivitas_el);

		input2.value = '';

		aktivitas_edit_el.addEventListener('click', (e) => {
			if (aktivitas_edit_el.innerText.toLowerCase() == "edit") {
				aktivitas_edit_el.innerText = "Save";
				aktivitas_input2_el.removeAttribute("readonly");
				aktivitas_input2_el.focus();
			} else {
				aktivitas_edit_el.innerText = "Edit";
				aktivitas_input2_el.setAttribute("readonly", "readonly");
			}
		})

		aktivitas_delete_el.addEventListener('click', (e) => {
			list_el2.removeChild(aktivitas_el);
		})
	})

    form.addEventListener('submit', (e) => {
		e.preventDefault();

		const kegiatan = input.value;

		const kegiatan_el = document.createElement('div');
		kegiatan_el.classList.add('kegiatan');

		const kegiatan_content_el = document.createElement('div');
		kegiatan_content_el.classList.add('content');

		kegiatan_el.appendChild(kegiatan_content_el);

		const kegiatan_input_el = document.createElement('input');
		kegiatan_input_el.classList.add('text');
		kegiatan_input_el.type = 'text';
		kegiatan_input_el.value = kegiatan;
		kegiatan_input_el.setAttribute('readonly', 'readonly');

		kegiatan_content_el.appendChild(kegiatan_input_el);

		const kegiatan_actions_el = document.createElement('div');
		kegiatan_actions_el.classList.add('actions');
		
		const kegiatan_edit_el = document.createElement('button');
		kegiatan_edit_el.classList.add('edit');
		kegiatan_edit_el.innerText = 'Edit';

		const kegiatan_delete_el = document.createElement('button');
		kegiatan_delete_el.classList.add('delete');
		kegiatan_delete_el.innerText = 'hapus';

		kegiatan_actions_el.appendChild(kegiatan_edit_el);
		kegiatan_actions_el.appendChild(kegiatan_delete_el);

		kegiatan_el.appendChild(kegiatan_actions_el);

		list_el.appendChild(kegiatan_el);

		input.value = '';

		kegiatan_edit_el.addEventListener('click', (e) => {
			if (kegiatan_edit_el.innerText.toLowerCase() == "edit") {
				kegiatan_edit_el.innerText = "Save";
				kegiatan_input_el.removeAttribute("readonly");
				kegiatan_input_el.focus();
			} else {
				kegiatan_edit_el.innerText = "Edit";
				kegiatan_input_el.setAttribute("readonly", "readonly");
			}
		})

		kegiatan_delete_el.addEventListener('click', (e) => {
			list_el.removeChild(kegiatan_el);
		})
	})

})